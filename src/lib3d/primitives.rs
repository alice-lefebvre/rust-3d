//! Cubes & spheres

use material::Material;

use crate::Mesh;
use crate::mesh::Face;
use crate::renderer::Shading;
use crate::*;

use std::f32::consts::PI;

pub fn cube() -> Mesh {
	let points = [
		Vec3::new(-1.0,  1.0, -1.0),
		Vec3::new(-1.0, -1.0, -1.0),
		Vec3::new( 1.0, -1.0, -1.0),
		Vec3::new( 1.0,  1.0, -1.0),

		Vec3::new(-1.0,  1.0,  1.0),
		Vec3::new(-1.0, -1.0,  1.0),
		Vec3::new( 1.0, -1.0,  1.0),
		Vec3::new( 1.0,  1.0,  1.0),
	];
	let faces = [
		Face::new_with_color(0, 2, 1, colors::YELLOW),
		Face::new_with_color(0, 3, 2, colors::YELLOW),

		Face::new_with_color(3, 6, 2, colors::RED),
		Face::new_with_color(3, 7, 6, colors::RED),

		Face::new_with_color(4, 1, 5, colors::CYAN),
		Face::new_with_color(4, 0, 1, colors::CYAN),
		
		Face::new_with_color(4, 3, 0, colors::GREEN),
		Face::new_with_color(4, 7, 3, colors::GREEN),

		Face::new_with_color(6, 1, 2, colors::MAGENTA),
		Face::new_with_color(6, 5, 1, colors::MAGENTA),

		Face::new_with_color(7, 5, 6, colors::WHITE),
		Face::new_with_color(7, 4, 5, colors::WHITE),
	];
	let mut cube = Mesh::new(Box::new(points), Box::new(faces));
	cube.shading = Shading::Flat;
	cube
}

pub fn textured_cube(material: Material) -> Mesh {
	let mut cube = cube();
	
	let width  = material.texture.as_ref().unwrap().image.get_width()  as f32 - f32::MIN_POSITIVE;
	let height = material.texture.as_ref().unwrap().image.get_height() as f32 - f32::MIN_POSITIVE;

	cube.set_material(material);
	cube.shading = Shading::Textured;

	let a = (0.0,   0.0);
    let b = (0.0,   height);
    let c = (width, height);
    let d = (width, 0.0);

    for (i, f) in cube.get_faces_mut().iter_mut().enumerate() {
        if i % 2 == 0 {
            f.texture_coord = [a, c, b];
        } else {
            f.texture_coord = [a, d, c];
        }
    }

	cube
}

pub fn octosphere(n: u32) -> Mesh {
	// let's try to make a spere.
	// idea is to make a octahedron, subdivise each face n times and project all the points on
	// a spehere or radius 1.
	// idea which is shamelessly taken from https://en.wikipedia.org/wiki/Geodesic_polyhedron
	// (we're doing octahedron bc it sounds easier but once it works we could do icosahedron)
	
	// build a single subdivided face which will be used to make the others

	let mut octa_faces = Vec::new();

	let angle = PI - 1.910_633_2; // arccos -1/3
	let height = 0.75_f32.sqrt(); // height of the triangle


	// building an octahedron
	let mut face1 = subdivided_triangle(n);
	face1.set_origin(Vec3::new(0.0, height, 0.0));
	face1.set_rotation(Mat3::rotation_x(angle/2.0));
	face1.apply_transformations_to_geometry();
	octa_faces.push(face1.clone());

	let mut previous_face = face1.clone();
	for _i in 0..3 {
		let mut new_face = previous_face.clone();
		new_face.set_rotation(Mat3::rotation_y(PI / 2.0));
		new_face.apply_transformations_to_geometry();
		octa_faces.push(new_face.clone());
		previous_face = new_face.clone();
	}
	
	let mut half_octa = Mesh::merge_meshes(octa_faces);
	half_octa.set_position(Vec3::new(0.0, 2f32.sqrt() / 2.0 -height, 0.0));
	half_octa.apply_transformations_to_geometry();
	let mut half_octa2 = half_octa.clone();
	half_octa2.set_origin(Vec3::new(0.0, 0.0, 0.0));
	half_octa2.set_rotation(Mat3::rotation_x(PI));
	half_octa2.apply_transformations_to_geometry();

	let mut sphere = Mesh::merge_meshes(vec![half_octa, half_octa2]);

	sphere.merge_vertices(0.0001);

	// projecting the subdivided octahedron onto sphere of radius 1
	for v in sphere.get_vertices_mut().iter_mut() {
		v.normalize();
	}

	sphere.bake_face_normals();
	sphere.bake_vertex_normals();
    sphere.calculate_bounding_box();

	sphere
}

/// n + 1 = number of subdivisions in a quadrant
pub fn boringsphere(n: u32) -> Mesh {
	let n = n + 1;

	let mut vertices = Vec::new();
	let mut faces = Vec::new();

	vertices.push(Vec3::new(0.0, 1.0, 0.0));

	for j in 1..2*n {
		let angle_y = PI * (j as f32 / n as f32);
		for i in 0..4*n+1 {
			let angle = 2.0 * PI * (i as f32/ n as f32);
			let v = Vec3::new(
				angle.cos() * angle_y.cos(),
				angle_y.sin(),
				angle.sin() * angle_y.cos());
			vertices.push(v);
			let idx = vertices.len() - 1;

			if j == 1 {
				if i > 0 {
					//faces.push(Face::new(idx, idx - 1, 0));
				}
			} else if i > 0 {
				faces.push(Face::new(idx, idx - 1, idx - 4 * n as usize - 2));
				faces.push(Face::new(idx, idx - 4 * n as usize - 2, idx - 4 * n as usize - 1));
			}
		}
	}


	Mesh::new(vertices.into_boxed_slice(), faces.into_boxed_slice())
}

/// returns a 1 unit squared plane centered on (0,0,0) and facing up. Its geometry is made by a (n+1) * (n+1) grid. For a simple plane, use n = 0
pub fn plane(n: u32) -> Mesh {
	let n = n + 1;
	let delta = 1.0/(n as f32);

	let mut vertices = Vec::new();
	let mut faces  = Vec::new();

	for j in 0..n+1 {
		for i in 0..n+1 {
			let x = 0.5 + delta * i as f32;
			let y = 0.5 + delta * j as f32;

			vertices.push(Vec3::new(y, 0.0, x));
			let idx = vertices.len() - 1;

			if j != 0 && i != 0{
				let grey = Color::new(0.5, 0.5, 0.5);
				faces.push(Face::new_with_color(idx, idx-1, idx - n as usize - 2, match (i+j) % 2 {0 => colors::WHITE, 1 => grey, _=> colors::CYAN}));
				faces.push(Face::new_with_color(idx, idx - n as usize - 2, idx - n as usize - 1, match (i+j) % 2 {0 => colors::WHITE, 1 => grey, _=> colors::CYAN}));
			}
		}
	}

	Mesh::new(vertices.into_boxed_slice(), faces.into_boxed_slice())
}

#[allow(unreachable_code)]
/*pub fn icosphere(_n: u32) -> Mesh{
	todo!(); // need to do *math*
	let angle = 1.0471975512; //sin-1(sqrt(3/4)) //angle between horizontal axis and height of a face
	let height = 0.75_f32.sqrt(); // height of the triangle face we're gonna use

	let mut face = subdivided_triangle(_n);
	face.set_origin(Vec3::new(0.0, height, 0.0));
	face.set_rotation(Mat3::rotation_x(angle)); //not the right angle proabably

	face

}*/

fn subdivided_triangle(n: u32) -> Mesh {
	let mut points = Vec::new();
	let mut faces  = Vec::new();

	let height = 0.75_f32.sqrt(); // height of the triangle

	let n = n + 2;

	for i in 0..n {
		let ratio = i as f32/(n as f32 - 1.0); // ratio of the line we draw compared to the triangle (we make the traingle top from bottom)
		let y = height * (1.0 - ratio);
		let line_len = ratio;
		for j in 0..i+1 {
			
			let x = if i == 0 {0.0} else {line_len * ((j as f32) / (i as f32)) - line_len/2.0};//- line_len / 2.0;

			points.push(Vec3::new(x, y, 0.0));
			let point_idx = points.len()-1;
			if i > 0 && j > 0 {
				faces.push(Face::new_with_color(point_idx, point_idx-1, point_idx - (i as usize) - 1, colors::YELLOW));
				if j < i {
					faces.push(Face::new_with_color(point_idx, point_idx - (i as usize) - 1, point_idx - (i as usize), colors::RED));
				}
			}
		}
	}

	Mesh::new(points.into_boxed_slice(), faces.into_boxed_slice())
}

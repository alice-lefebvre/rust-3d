//! Rendering ! Transforming the 3d world into a 2d image
//! 
//! [Renderer] is where the magic happens

use std::mem::swap;

use std::cmp::min;
use std::rc::Rc;
use std::time::Instant;
use crate::{*, mesh::Face};
use crate::lighting::Lighting;
use crate::material::Material;
use crate::mesh::Mesh;
use l2d::{Pixel, RasterizedTriangle};
use material::Texture;

#[derive(PartialEq)]
enum Comp {
	GreaterThan,
	LessThan,
}

#[derive(PartialEq)]
enum Direction {
	Horizontal,
	Vertical,
}

#[derive(Clone, PartialEq)]
pub enum RenderMode {
	Solid,
	Wireframe,
}

#[derive(Clone)]
pub enum Shading {
	Flat,
	Smooth,
	Textured,
}

/// This trait informs that an object is able to be drawn on a buffer, via the [Renderable::render()] function
pub trait Renderable {
    // Returns the number of triangles drawn
	fn render(&self, renderer: &mut Renderer, lighting: Option<&Lighting>) -> Option<u32>;
	fn get_shading_mut(&mut self) -> Option<&mut Shading> {
		None
	}
	fn get_render_mode_mut(&mut self) -> Option<&mut RenderMode> {
		None
	}
}

/// Will do all the rendering for you, free of charge
/// 
/// The camera is placed at (0, 0, 0) and looks into the -z direction. To get camera movement, you need
/// to translate and rotate all the objects that are viewed through that camera (this is what the trait [trans::InCamera] is for)
pub struct Renderer {
	/// Contains the actual pixels
	image_buffer: l2d::Buffer<u32>,
	/// Holds depth information for drawing on top of an already rendered part of the image
	depth_buffer: l2d::Buffer<f32>,
	width : u32,
	height : u32,
	/// Allows you to change the focal length used by the renderer. This is the tangent of the FOV (i believe).
	pub focal_length: f32,
	/// The range of values that will be captured when rendering.
	/// 
	/// Anything lower that dynamic_range.0 will be underexposed, and anything higher than dynamic_range.1 will be overexposed
	pub dynamic_range: (f32, f32),
	pub times_debug: (u128, u128, u128, u128),
    pub near_plane: f32,
    pub render_mode: RenderMode,
}

impl Renderer {
	pub fn new(width: u32, height: u32) -> Renderer {
		Renderer {
			image_buffer: l2d::Buffer::<u32>::new(width, height),
			depth_buffer: l2d::Buffer::<f32>::new(width, height),
			width,
			height,
			focal_length: 1.0,
			dynamic_range: (0.0, 1.0),
			times_debug: (0, 0, 0, 0),
            near_plane: 0.5,
            render_mode: RenderMode::Solid,
		}
	}

	/// Blank the image buffer with black, and resets the depth buffer.
	pub fn blank(&mut self) {
		self.image_buffer.fill(0);
        self.depth_buffer.fill(f32::MAX);
	}

	pub fn blank_with_color(&mut self, color: l2d::Color) {
		self.image_buffer.fill(color.value);
        self.depth_buffer.fill(f32::MAX);
	}

	/// Get the rendered image
	pub fn get_image(&self) -> &l2d::Buffer<u32> {
		&self.image_buffer
	}

	// useful for debug (show fps & stuff) that might be done differently later (copy the buffer ? sounds long)
	pub fn get_image_mut(&mut self) -> &mut l2d::Buffer<u32> {
		&mut self.image_buffer
	}

	pub fn get_depth_buffer(&self) -> &l2d::Buffer<f32> {
		&self.depth_buffer
	}
    
    pub fn get_depth_buffer_mut(&mut self) -> &mut l2d::Buffer<f32> {
        &mut self.depth_buffer
    }

    pub fn project(&self, v: &Vec3) -> Pixel {
        v.project(self.focal_length, self.width, self.height)
    }

	/// Draws the mesh onto the image buffer, while updating the z-buffer
	///
	/// This function does not return an image. To get an image, draw all your meshes
	/// then use [get_image](Renderer::get_image)
	/// 
	/// returns the number of drawn triangles
	pub fn draw_3d_mesh<'a> (
		&mut self,
		mesh: &Mesh,
		lighting: Option<&Lighting>,
	) -> Option<u32> {
		let time = Instant::now();
	    
        if mesh.frustum_culling {
            let mut culled = true;
            for v in mesh.bounding_box.vertices_transformed.iter() {
                let p = self.project(&v);
                if self.image_buffer.is_valid(&p) {
                    culled = false;
                    break;
                }
            }

            if culled {
                return None
            }
        }

		let mut tri_count = 0_u32;
        //clone :(
		let mut points: Vec<Vec3> = mesh.get_vertices_for_rendering().to_vec();
        let mut vertex_normals: Vec<Vec3> = mesh.get_vertex_normals().to_vec();
		self.times_debug.0 += time.elapsed().as_nanos();
        
        // not great for perf but not *that* bad. i can't find an elegant way to keep that to one
        // loop, but it's definitely possible
        let mut faces_to_render: Vec<Face> = Vec::with_capacity(mesh.get_faces().len() + 64);

        // I hate that function
        near_plane_clipping(mesh, self.near_plane, &mut points, &mut vertex_normals, &mut faces_to_render);

        let mut projected = Vec::with_capacity(points.len() + 64);
		for p in points.iter() {
			// known issue, for points that are extremely out of the field of view,
			// this will cause an overflow.
			let pixel = p.project(self.focal_length,self.width, self.height);
			projected.push(pixel);
			// pixel_buffer.draw_pixel(pixel, color);
		}

		for face in faces_to_render {
			//time = Instant::now();
			let &a = &points[face.vtx_id[0]];
			let &b = &points[face.vtx_id[1]];
			let &c = &points[face.vtx_id[2]];

			//on assume camera_pos = 0,0,0
			let mut camera_vec = Vec3::center_of_triangle(a, b, c);
			let center = camera_vec;
			
		
			//let camera_pos = Point::new(0 as f32, 0 as f32, -1 as f32);
			camera_vec.normalize().invert();
			let camera_dp = Vec3::dot_product(face.normal_trans, camera_vec);
			
			// only draw possibly visible triangles (normals facing the camera)
			if camera_dp >= 0.0 || !mesh.backface_culling {	
				let zvalue = (a.z + b.z + c.z) / 3.0; //only useful on simple zbuffer mode
				// let new_color = l2d::colors::WHITE;
				let new_color;

				let &p0 = &projected[face.vtx_id[0]];
				let &p1 = &projected[face.vtx_id[1]];
				let &p2 = &projected[face.vtx_id[2]];

                if !self.image_buffer.is_valid(&p0) && !self.image_buffer.is_valid(&p1) && !self.image_buffer.is_valid(&p2) {
                    // good optimization, but prevents the drawing of a triangle which have points
                    // outside the canvas but still should be seen (big shapes)
                    // continue;
                }

				let mut projected_triangle = l2d::RasterizedTriangle::new(
					l2d::ProjectedPoint::from_point_with_tc(p0, a.z, face.texture_coord[0]),
					l2d::ProjectedPoint::from_point_with_tc(p1, b.z, face.texture_coord[1]),
					l2d::ProjectedPoint::from_point_with_tc(p2, c.z, face.texture_coord[2]),
				);

				#[allow(clippy::unnecessary_unwrap)] // pas envie de faire deux conditions, let if let ne marche pas
                if lighting.is_some() && mesh.render_mode != RenderMode::Wireframe {
					let lighting = lighting.unwrap();
				    match mesh.shading {
					    Shading::Flat => {
						    new_color = self.process_lighting(lighting, &face.material, center, face.normal_trans);
					    },
					    _ => {
						    new_color = l2d::colors::BLACK;
						    let mut points = projected_triangle.get_points_mut();
						
						    points[0].color = self.process_lighting(lighting, &face.material, a, vertex_normals[face.vtx_id[0]]);
						    points[1].color = self.process_lighting(lighting, &face.material, b, vertex_normals[face.vtx_id[1]]);
						    points[2].color = self.process_lighting(lighting, &face.material, c, vertex_normals[face.vtx_id[2]]);

					    }
				    }
                }
                else {
                    new_color = l2d::colors::BLACK;
                }

				projected_triangle.sort_y();


				let mut tris_to_render = Vec::new();

				if !self.image_buffer.is_valid(&p0) || !self.image_buffer.is_valid(&p1) || !self.image_buffer.is_valid(&p2) {
					// self.clipping_test(pixel_buffer, projected_triangle);
					tris_to_render = self.clipping_main(projected_triangle);
				} else {
					tris_to_render.push(projected_triangle);
				}
				
				//self.times_debug.1 += time.elapsed().as_nanos();
				//time = Instant::now();
                for t in tris_to_render {
                    tri_count += 1;
                    match self.render_mode {
                        RenderMode::Solid => {
                            match mesh.render_mode {
                                RenderMode::Solid => {
                                    match mesh.shading {
                                        Shading::Flat => self.draw_shaded (
                                            mesh,
                                            zvalue,
                                            &t,
                                            new_color.value,
                                        ),
                                        Shading::Smooth => self.draw_smooth_shaded (
                                            mesh,
                                            zvalue,
                                            &t,
                                            new_color.value,
                                        ),
                                        Shading::Textured => self.draw_textured (
                                            &t,
                                            &face.material.texture,
                                        ),
                                    }
                                },
                                RenderMode::Wireframe => self.draw_wireframe(
                                    &t,
                                ),
                            }
                        },
                        RenderMode::Wireframe => {
                            self.draw_wireframe(&t);
                        }
                    }

                    //self.draw_wireframe(
                    //&t,
                    //);
                }

				//self.times_debug.2 += time.elapsed().as_nanos();

				/*self.draw_normal(mesh.get_vertex_normals()[face.vtx_id[0]], a);
				self.draw_normal(mesh.get_vertex_normals()[face.vtx_id[1]], b);
				self.draw_normal(mesh.get_vertex_normals()[face.vtx_id[2]], c);*/
			}
		};

		Some(tri_count)
	}

	// bad z buffering for now, 1 value for the whole triangle
	// use barycentric coordinates to interpolate z values on the surface of the triangle
	// see : https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/barycentric-coordinates
	// for now this function repeats code from l2d draw solid triangle

	// ok now uses 2d barycentric coordinates => better but not as good as 3d barycentric coord.

	//arguments are weird, trying optimization stuff
	fn draw_shaded(
		&mut self,
		mesh: &Mesh,
		zvalue: f32,
		tri: &l2d::RasterizedTriangle,
		value: u32)
	{
		let points_zvalue = tri.get_points();
		let tri_2d = tri.to_triangle();
		let points_2d = tri_2d.get_points();

		let p0 = points_2d[0];
		let p1 = points_2d[1];
		let p2 = points_2d[2];

		let x02 = l2d::interpolate(&p0.y, &p0.x, &p2.y, &p2.x);
		let mut x012 = l2d::interpolate(&p0.y, &p0.x, &p1.y, &p1.x);
		x012.remove(x012.len() - 1);
		x012.extend(l2d::interpolate(&p1.y, &p1.x, &p2.y, &p2.x));

		let m = (p1.y - p0.y) as usize; // values are sorted on x so is ok ez

		let x_left;
		let x_right;

		// if some panic happens here, make sure your triangle points are sorted on the Y axis :)

		if x02[m] < x012[m] {
			x_left = x02;
			x_right = x012;
		} else {
			x_left = x012;
			x_right = x02;
		}
		
		//let t = tri.to_triangle();
		let area = tri_2d.get_area();

		for y in p0.y..p2.y + 1 {
			// println!("y: {}", y);
			let slice = (y - p0.y) as usize;
			for x in x_left[slice]..x_right[slice] + 1 {
				let current_point = l2d::Pixel::new(x, y);

				let mut zvalue = zvalue;

				if mesh.accurate_zbuffer {

					// if area = 0 maybe still draw the triangle as a line. Zvalues linearly interpolated.
					if area > 0.0 {
						let bary = tri_2d.get_barycentric_coord_opt(&current_point, &area);

						zvalue = 
					    (
							points_zvalue[0].z * bary.u +
							points_zvalue[1].z * bary.v +
							points_zvalue[2].z * bary.w
						) / (bary.u + bary.v + bary.w);
						
					} else {
						continue;
					}
				}
				
				let s = self.depth_buffer.get_value_no_check(&current_point);

				if zvalue <= s {
					self.image_buffer.draw_pixel_no_check(&current_point, value);
					self.depth_buffer.draw_pixel_no_check(&current_point, zvalue);
				}
			}
		}
	}

	fn draw_smooth_shaded(
		&mut self,
		mesh: &Mesh,
		zvalue: f32,
		tri: &l2d::RasterizedTriangle,
		color: u32)
	{
		let points_zvalue = tri.get_points();
		let tri_2d = tri.to_triangle();
		let points_2d = tri_2d.get_points();
		let mut color = color;

		let p0 = points_2d[0];
		let p1 = points_2d[1];
		let p2 = points_2d[2];

		let x02 = l2d::interpolate(&p0.y, &p0.x, &p2.y, &p2.x);
		let mut x012 = l2d::interpolate(&p0.y, &p0.x, &p1.y, &p1.x);
		x012.remove(x012.len() - 1);
		x012.extend(l2d::interpolate(&p1.y, &p1.x, &p2.y, &p2.x));

		let m = (p1.y - p0.y) as usize; // values are sorted on x so is ok ez

		let x_left;
		let x_right;

		// if some panic happens here, make sure your triangle points are sorted on the Y axis :)

		if x02[m] < x012[m] {
			x_left = x02;
			x_right = x012;
		} else {
			x_left = x012;
			x_right = x02;
		}

		//let t = tri.to_triangle();
		let area = tri_2d.get_area();

		let image_buffer = self.image_buffer.get_buffer_mut();
		let depth_buffer = self.depth_buffer.get_buffer_mut();

		// let xmin = std::cmp::min(p0.x, std::cmp::min(p1.x, p2.x));
		// let xmax = std::cmp::max(p0.x, std::cmp::max(p1.x, p2.x));

		for y in p0.y..p2.y + 1 {
			// println!("y: {}", y);
			let slice = (y - p0.y) as usize;
			for x in x_left[slice]..x_right[slice] + 1 {
				let current_point = l2d::Pixel::new(x, y);

				let mut zvalue = zvalue;

				// let mut color = l2d::Color { value: 0x000000 }; 
				let r;
				let g;
				let b;

				if mesh.accurate_zbuffer {

					// if area = 0 maybe still draw the triangle as a line. Zvalues linearly interpolated.
					if area > 0.0 {
						let bary = tri_2d.get_barycentric_coord_opt(&current_point, &area);

						let sum_bary = bary.u + bary.v + bary.w;

						zvalue = 
					    (
							points_zvalue[0].z * bary.u +
							points_zvalue[1].z * bary.v +
							points_zvalue[2].z * bary.w
						) / sum_bary;

						r = (
							points_zvalue[0].color.r() as f32 * bary.u +
							points_zvalue[1].color.r() as f32 * bary.v +
							points_zvalue[2].color.r() as f32 * bary.w
						) / sum_bary;

						g = (
							points_zvalue[0].color.g() as f32 * bary.u +
							points_zvalue[1].color.g() as f32 * bary.v +
							points_zvalue[2].color.g() as f32 * bary.w
						) / sum_bary;

						b = (
							points_zvalue[0].color.b() as f32 * bary.u +
							points_zvalue[1].color.b() as f32 * bary.v +
							points_zvalue[2].color.b() as f32 * bary.w
						) / sum_bary;

						

						color = (r  as u32) << 16 | (g  as u32) << 8 | b  as u32;
						//funny color
						//color = r << 16 + g * 0x000100 + b
						//color = b * r * g // < best one yet

					} else {
						continue;
					}
				}
				
				let index = (current_point.y * self.width as i32 + current_point.x) as usize;
				
				let s = depth_buffer[index];

				if zvalue <= s {
					image_buffer[index] = color;
					depth_buffer[index] = zvalue;
				}

				/*let s = self.depth_buffer.get_value_no_check(&current_point);

				if zvalue <= s {
					self.image_buffer.draw_pixel_no_check(&current_point, color);
					self.depth_buffer.draw_pixel_no_check(&current_point, zvalue);
				}*/
			}
		}
	}

	// in the near future, combine all functions here. Branch just before the 2 inner loops if necessary
	fn draw_textured(
		&mut self,
		tri: &l2d::RasterizedTriangle,
		texture: &Option<Rc<Texture>>,
	) {
		let projected_points = tri.get_points();
		//let tri_2d = tri.to_triangle();
		//let points_2d = tri_2d.get_points();

		let p0 = projected_points[0];
		let p1 = projected_points[1];
		let p2 = projected_points[2];

		let x02 = l2d::interpolate(&p0.y, &p0.x, &p2.y, &p2.x);
		let mut x012 = l2d::interpolate(&p0.y, &p0.x, &p1.y, &p1.x);
		x012.remove(x012.len() - 1);
		x012.extend(l2d::interpolate(&p1.y, &p1.x, &p2.y, &p2.x));

		let m = (p1.y - p0.y) as usize; // values are sorted on x so is ok ez

		let x_left;
		let x_right;

		// if some panic happens here, make sure your triangle points are sorted on the Y axis :)

		if x02[m] < x012[m] {
			x_left = x02;
			x_right = x012;
		} else {
			x_left = x012;
			x_right = x02;
		}

		let tri_2d = tri.to_triangle();
		let area = tri_2d.get_area();

		let image_buffer = self.image_buffer.get_buffer_mut();
		let depth_buffer = self.depth_buffer.get_buffer_mut();

		let mut textured = true;
		let mut texture_width = 0;
		let mut texture_height = 0;

		match texture.as_ref() {
			None => {
				textured = false;
			}
			Some(t) => {
				let texture_unwrapped = t;
				texture_width = texture_unwrapped.image.get_width() as i32;
				texture_height = texture_unwrapped.image.get_height() as i32;
			}
		}

		
		

		// if area = 0 maybe still draw the triangle as a line. Zvalues linearly interpolated.
		if area > 0.0 {
			for y in p0.y..p2.y + 1 {
				// println!("y: {}", y);
				let slice = (y - p0.y) as usize;
				for x in x_left[slice]..x_right[slice] + 1 {
					let current_point = l2d::Pixel::new(x, y);

					let point = tri.interpolate_vertices_at_pos(&current_point, area);
					let mut color = point.color;


					if textured {
						// println!("{}, {}", point.tc.0, point.tc.1);
						let tcolor = texture.as_ref().unwrap().image.get_value_no_check(&Pixel::new(point.tc.0 as i32 % texture_width, point.tc.1 as i32 % texture_height));
						color = tcolor * point.color;
					}
					// println!("{}", coordx);

					//color = (r  as u32) << 16 | (g  as u32) << 8 | b  as u32;
					//funny color
					//color = r << 16 + g * 0x000100 + b
					//color = b * r * g // < best one yet

					let index = (current_point.y * self.width as i32 + current_point.x) as usize;
				
					let s = depth_buffer[index];

					if point.z <= s {
						image_buffer[index] = color.value;
						depth_buffer[index] = point.z;
					}
					
					

					/*let s = self.depth_buffer.get_value_no_check(&current_point);

					if zvalue <= s {
						self.image_buffer.draw_pixel_no_check(&current_point, color);
						self.depth_buffer.draw_pixel_no_check(&current_point, zvalue);
					}*/
				}
			}
		}
	}

	fn draw_wireframe(
		&mut self,
		tri: &l2d::RasterizedTriangle,)
	{
		self.image_buffer.draw_triangle(
			&tri.to_triangle(),
			l2d::colors::WHITE.value,
		)
	
	}

	#[allow(dead_code)]
	fn draw_normal(
		&mut self,
		normal: Vec3,
		pos: Vec3,
	) {
		let start = pos;
		let end = pos + normal / 6.0;

		let start = start.project(self.focal_length, self.width, self.height);
		let end   = end.project(self.focal_length, self.width, self.height);
		if self.image_buffer.is_valid(&start) && self.image_buffer.is_valid(&end) {
			self.image_buffer.draw_line(start, end, l2d::colors::RED.value);
		}
	}

	/// Calculate the color value of one point in 3d space from the color information at that point and lighting
	/// 
	/// Tone mapping occurs here.
	/// 
	/// # returns
	/// 
	/// A 24-bit color value, tone-mapped from the real color value
	/// 
	/// # args
	/// 
	/// * `lighting` - The lighting that will be use to process that point's color
	/// * `color` - The surface color of the point. Typically will be the face color at that point
	/// * `point` - The point at which we calculate the lighting. This only has an influence on point lights
	/// * `normal` - The normal at that point. This is useful to know how much the point faces each light source,
	/// allowing us to know how much the light influences the color
	/// 
	fn process_lighting(&self, lighting: &Lighting, material: &Material, point: Vec3, normal: Vec3) -> l2d::Color {
		
		if material.emissive {
			let r = min((material.color.r * 255.0) as u32, 255);
			let g = min((material.color.g * 255.0) as u32, 255);
			let b = min((material.color.b * 255.0) as u32, 255);
		
			let r = r as u8;
			let g = g as u8;
			let b = b as u8;
	
			return l2d::Color::from_rgb(r, g, b);
		}

		// ambiant lighting
		let mut r = lighting.ambiant.color.r * lighting.ambiant.intensity;
		let mut g = lighting.ambiant.color.g * lighting.ambiant.intensity;
		let mut b = lighting.ambiant.color.b * lighting.ambiant.intensity;
		
		
		// global lighting (parallel)
		for sun in lighting.suns.iter() {
			let dot_product = Vec3::dot_product(normal, *sun.get_vector());
			
			if dot_product > 0.0 {
				r += sun.color.r * sun.intensity * dot_product;
				g += sun.color.g * sun.intensity * dot_product;
				b += sun.color.b * sun.intensity * dot_product;
			} 
		}
		
		// point lights
		for lamp in lighting.lamps.iter() {
			let distance = Vec3::distance(&point, lamp.get_position());
			if distance < lamp.distance {
				let ray_vector = *lamp.get_position() - point;
				let dot_product = Vec3::dot_product(normal, ray_vector);
				

				if dot_product > 0.0 {
					r += lamp.color.r * lamp.intensity * (dot_product / distance.powf(2.0));
					g += lamp.color.g * lamp.intensity * (dot_product / distance.powf(2.0));
					b += lamp.color.b * lamp.intensity * (dot_product / distance.powf(2.0));
				}
			}
		}
	
		// couleur de l'objet
		r *= material.color.r;
		g *= material.color.g;
		b *= material.color.b;
		
		// clipping down to acceptable values
		// different exposures should be handled here
		let mut r = (((r / self.dynamic_range.1) - self.dynamic_range.0).sqrt() * 255.0).floor() as u32;
		let mut g = (((g / self.dynamic_range.1) - self.dynamic_range.0).sqrt() * 255.0).floor() as u32;
		let mut b = (((b / self.dynamic_range.1) - self.dynamic_range.0).sqrt() * 255.0).floor() as u32;
		
		r = min(r, 255);
		g = min(g, 255);
		b = min(b, 255);
	
		let r = r as u8;
		let g = g as u8;
		let b = b as u8;
	
		l2d::Color::from_rgb(r, g, b)
	}

    // I think we should do that in 3D space
	fn clipping_main(&self, tri: RasterizedTriangle) -> Vec<RasterizedTriangle> {
		let clipped_tris = clipping(0             , Comp::GreaterThan, Direction::Horizontal, vec![tri]);
		let clipped_tris = clipping(self.width -1 , Comp::LessThan   , Direction::Horizontal, clipped_tris);
		let clipped_tris = clipping(0             , Comp::GreaterThan, Direction::Vertical  , clipped_tris);
		clipping(self.height -1, Comp::LessThan   , Direction::Vertical  , clipped_tris)
	}
	/* 
	pub fn draw_points(pixel_buffer: &mut l2d::Buffer::<u32>, d: f32, color: l2d::Color) {
		for p in self.points.iter() {
			pixel_buffer.draw_pixel(p.project(d, pixel_buffer.get_width(), pixel_buffer.get_height()), color.value);
		}
	} */
	
	
}

/// Clipping the projected triangles against the edges of the screen
///
/// # Arguments
/// 
/// `limit` - what value to clip against
/// `comp` - wether the acceptable vales are the ones greater or smaller than limit
/// `direction` - tells if we need to clip on the X-axis or the Y-axis
/// `vec` - vector of the triangles to clip. The clipped triangles are removed and replaces by acceptable ones
/// 
/// yes this function is aweful
fn clipping(limit: u32, comp: Comp, direction: Direction, vec: Vec<RasterizedTriangle>) -> Vec<RasterizedTriangle> {
	let sign = match comp {Comp::GreaterThan => 1, Comp::LessThan => -1};
	let limit = limit as i32;
	let mut result = Vec::new();

	// when reading this code, keep in mind that this was written for clipping on the LEFT side
	// and then modified to fit other cases.

	for tri in vec {
		let mut points = *tri.get_points(); // maybe dont in the final version, use mut to avoid new allocation (for now needed bc sort)

		// this is kind of aweful but no chance im gonna rewrite all this junk while inversing x and y
		if direction == Direction::Vertical {
			for p in points.iter_mut() {
				swap(&mut p.x, &mut p.y);
			}
		}

		points.sort_by_key(|k| sign * k.x);

		let a = points[0];
		let b = points[1];
		let c = points[2];

		let tri_sorted = RasterizedTriangle::new(a, b, c);

		match count_points_outside_x_limit(vec![a.to_point(), b.to_point(), c.to_point()], limit, sign) {

			3 => {
                continue; // triangle is entirely outside the plane, discard the triangle.
			},
			0 => {
				result.push(tri);
				continue; // triangle is entirely inside the plane, add the triangle without clipping.
			},
			1 => {
				// 1 dehors

				//     x = 0
				//       |
				//       |     b
				//  a    |
				//       |       c
				//       |

				// line coefficient
				let d = if b.x != a.x {(b.y - a.y) as f32 / (b.x - a.x) as f32} else {0_f32};
				// p0 = intersection between A, B and x = limit
				let p0 = Pixel::new(limit, a.y - (d * (a.x + limit * sign) as f32).round() as i32);
			
				//let bary = triangle.get_barycentric_coord_opt(&p0, area);
				
				//let z = (bary.u * a.z + bary.v * b.z + bary.w * c.z) / (bary.u + bary.v + bary.w);
				//let color = color_from_point_inside_projected_triangle(a, b, c, bary);
				// let mut p0 = ProjectedPoint::from_point(p0, z);
				let p0 = tri_sorted.interpolate_vertices_at_pos(&p0, tri.get_area());
				//p0.color = color;

				// P1
				
				let d = if c.x != a.x {(c.y - a.y) as f32 / (c.x - a.x) as f32} else {0_f32};
				let p1 = Pixel::new(limit, a.y - (d * (a.x + limit * sign) as f32).round() as i32);
				let p1 = tri_sorted.interpolate_vertices_at_pos(&p1, tri.get_area());

				let mut projected_t1 = RasterizedTriangle::new(p0, b, c);
				let mut projected_t2 = RasterizedTriangle::new(p1, p0, c);

				if direction == Direction::Vertical {
					for p in projected_t1.get_points_mut().iter_mut() {
						swap(&mut p.x, &mut p.y);
					}
					for p in projected_t2.get_points_mut().iter_mut() {
						swap(&mut p.x, &mut p.y);
					}
				}

				projected_t1.sort_y();
				projected_t2.sort_y();

				result.push(projected_t1);
				result.push(projected_t2);
				
			},
			2 => {
				// 2 points hors du cadre

				// coefficient de la droite
				let d = if c.x != a.x {(c.y - a.y) as f32 / (c.x - a.x) as f32} else {0_f32};
				// p0 = intersection de a, c avec l'axe x = limit
				let p0 = Pixel::new(limit, a.y - (d * (a.x + limit * sign) as f32).round() as i32);
				let p0 = tri_sorted.interpolate_vertices_at_pos(&p0, tri.get_area());

				let d = if c.x != b.x {(c.y - b.y) as f32 / (c.x - b.x) as f32} else {0_f32};
				let p1 = Pixel::new(limit, b.y - (d * (b.x + limit * sign) as f32).round() as i32);
				let p1 = tri_sorted.interpolate_vertices_at_pos(&p1, tri.get_area());

				let mut projected_t1 = RasterizedTriangle::new(p0, p1, c); //unsorted bc we sort later

				if direction == Direction::Vertical {
					for p in projected_t1.get_points_mut().iter_mut() {
						swap(&mut p.x, &mut p.y);
					}
				}

				projected_t1.sort_y();

				result.push(projected_t1);
			}
			_ => panic!("??")
		}
	}

	result
}

fn near_plane_clipping(mesh: &Mesh, near_plane: f32, points: &mut Vec<Vec3>, vertex_normals: &mut Vec<Vec3>, faces_to_render: &mut Vec<Face>) {
    for face in mesh.get_faces().iter() {
        let &a = &points[face.vtx_id[0]];
        let &b = &points[face.vtx_id[1]];
        let &c = &points[face.vtx_id[2]];

        if a.z <= near_plane && b.z <= near_plane && c.z <= near_plane {
            continue;
        }
        // Near plane clipping
        if a.z <= near_plane || b.z <= near_plane || c.z <= near_plane {
            // does the double pointer do anything? who knows. Idea is to not do new
            // assignation (we need index for after the sort)
            let mut arr: [(usize, &&Vec3); 3] = [(face.vtx_id[0], &&a), (face.vtx_id[1], &&b), (face.vtx_id[2], &&c)];
            arr.sort_by(|a, b| a.1.z.partial_cmp(&b.1.z).unwrap());
            // only 2 cases: 1 or 2 points on the wrong side of the near plane
            let n_points_to_clip = if arr[1].1.z > near_plane { 1 } else { 2 }; 
            if n_points_to_clip == 2 {
                let newp1 = line_zplane_intersect(arr[0].1, arr[2].1, near_plane);
                let newp2 = line_zplane_intersect(arr[1].1, arr[2].1, near_plane);
                //let's pray no /0 happens here, i can't be bothered to check
                let ratio1 = Vec3::distance(&newp1, arr[0].1) / Vec3::distance(&arr[0].1, &arr[2].1);
                let ratio2 = Vec3::distance(&newp2, arr[1].1) / Vec3::distance(&arr[1].1, &arr[2].1);
                points.push(newp1);
                let newp1_idx = points.len() - 1;
                vertex_normals.push(linear_interpolation_vec3(&vertex_normals[arr[0].0], &vertex_normals[arr[2].0], ratio1));
                points.push(newp2);
                let newp2_idx = points.len() - 1;
                vertex_normals.push(linear_interpolation_vec3(&vertex_normals[arr[1].0], &vertex_normals[arr[2].0], ratio2));
                let mut new_face = Face::new(arr[2].0, newp2_idx, newp1_idx);
                new_face.material = face.material.clone();
                new_face.normal_trans = face.normal_trans;
                faces_to_render.push(new_face);
                continue;
            } else {
                // continue;
                let newp1 = line_zplane_intersect(arr[0].1, arr[1].1, near_plane);
                let newp2 = line_zplane_intersect(arr[0].1, arr[2].1, near_plane);
                let ratio1 = Vec3::distance(&newp1, arr[0].1) / Vec3::distance(&arr[0].1, &arr[1].1);
                let ratio2 = Vec3::distance(&newp2, arr[0].1) / Vec3::distance(&arr[0].1, &arr[2].1);
                points.push(newp1);
                vertex_normals.push(linear_interpolation_vec3(&vertex_normals[arr[0].0], &vertex_normals[arr[1].0], ratio1));
                let newp1_idx = points.len() - 1;
                points.push(newp2);
                vertex_normals.push(linear_interpolation_vec3(&vertex_normals[arr[0].0], &vertex_normals[arr[2].0], ratio2));
                let newp2_idx = points.len() - 1;
                let mut face1 = Face::new(newp1_idx, arr[2].0, arr[1].0);
                let mut face2 = Face::new(newp1_idx, newp2_idx, arr[2].0);
                face1.material = face.material.clone();
                face2.material = face.material.clone();
                face1.normal_trans = face.normal_trans;
                face2.normal_trans = face.normal_trans;
                faces_to_render.push(face1);
                faces_to_render.push(face2);
                continue;
            }
        }
        faces_to_render.push(face.clone());
    }
}

fn count_points_outside_x_limit(points: Vec<Pixel>, limit: i32, sign: i32) -> u8 {
	let mut count = 0;
	for p in points {
		if p.x * sign < limit * sign {
			count += 1;
		}
	}

	count
}


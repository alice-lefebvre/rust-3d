//! Let there be light

use crate::trans::{Transformation};
use crate::*;
pub use crate::trans::InCamera;

/// Point light
pub struct Lamp {
	pos: Vec3,
	pos_trans: Vec3,
	pub color: Color,
	pub intensity: f32,
	/// Distance at which we stop factoring in the effect of the light on a surface. Computed when using new() to create a lamp.
	pub distance: f32,
}

impl Lamp {
	pub fn new(pos: Vec3, color: Color, intensity: f32) -> Lamp {

		let threshold = 0.001; // portion of the light we consider negligeable
		let distance = (intensity / threshold).sqrt();

		Lamp {
			pos,
			pos_trans: pos,
			color,
			intensity,
			distance
		}
	}

	pub fn set_position(&mut self, pos: Vec3) {
		self.pos = pos;
	}

	pub fn get_position(&self) -> &Vec3 {
		&self.pos_trans
	}
}

/// This is a directional light (infinitely far lightsource; all rays are parallel)
pub struct Sun {
	vec: Vec3,
	vec_trans: Vec3,
	pub color: Color,
	pub intensity: f32,
}

impl Sun {
	pub fn new(vec: Vec3, color: Color, intensity: f32) -> Sun {
		let mut vec = vec;
		vec.normalize();
		Sun {
			vec,
			vec_trans: vec,
			color,
			intensity
		}
	}

	pub fn get_vector(&self) -> &Vec3 {
		&self.vec_trans
	}
}

impl InCamera for Sun {
	fn transform(&mut self, camera_trans: &Transformation) {
		self.vec_trans = camera_trans.rotation * self.vec;
		// no need for translation since we're infinitely far away
	}
}

impl InCamera for Lamp {
	fn transform(&mut self, camera_trans: &Transformation) {
		let pos_trans = camera_trans.translation + self.pos;
		self.pos_trans = camera_trans.rotation * pos_trans;
	}
}

pub struct AmbiantLighting {
	pub color: Color,
	pub intensity: f32,
}

impl AmbiantLighting {
	pub fn new(color: Color, intensity: f32) -> AmbiantLighting {
		AmbiantLighting {color, intensity}
	}
}

pub struct Lighting {
	pub suns: Vec<Sun>,
	pub lamps: Vec<Lamp>,
	pub ambiant: AmbiantLighting,
}

#[allow(clippy::new_without_default)]
impl Lighting{
	pub fn new() -> Lighting {
		Lighting {
			suns: Vec::<Sun>::new(),
			lamps: Vec::<Lamp>::new(),
			ambiant: AmbiantLighting::new(colors::BLACK, 0.0),
		}
	}
}
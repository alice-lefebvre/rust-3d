//! Objects transformation and camera panning & movement

use crate::*;

/// A Transformation in 3D space. We start from the origin, rotate the object with the rotation matrix,
/// then move the object according to the translation vector.  
#[derive(Clone)]
pub struct Transformation {
    pub translation:Vec3, // too lazy to do this 4d matrix stuff + doesnt seem that much more convenient
    pub rotation:Mat3, // think this could also do scaling but who cares about scaling am i right
}

impl Transformation {
    pub fn new(trans: Vec3, rot: Mat3) -> Transformation {
        Transformation {
            translation: trans,
            rotation: rot,
        }
    }

    pub fn translate(&mut self, t: Vec3) {
        self.translation = self.translation + t;
    }

    /*pub fn invert(&mut self) -> Transformation{
        Transformation::new(0 - self.trans, rot)
    }*/
}

impl Default for Transformation {
    fn default() -> Transformation {
        Transformation {
            translation: Vec3::default(),
            rotation: Mat3::identity(),
        }
    }
}

/// This traits tells us the entity can be transformed (position + rotation)
pub trait Transformable {
    fn get_transformation(&mut self) -> &mut Transformation;

    fn set_position(&mut self, pos: Vec3) {
        self.get_transformation().translation = pos;
        self.after_transformation();
    }

    fn set_rotation(&mut self, rot: Mat3) {
        self.get_transformation().rotation = rot;
        self.after_transformation();
    }

    /// DOESNT WORK
    #[deprecated]
    fn rotate(&mut self, rot: Vec3) {
        //ordre z, y , x
        self.get_transformation().rotation = Mat3::rotation_z(rot.z) * Mat3::rotation_y(rot.y) * Mat3::rotation_x(rot.x) * self.get_transformation().rotation;
        self.after_transformation();
    }

    fn translate(&mut self, vec: Vec3) {
        self.get_transformation().translation = self.get_transformation().translation + vec;
        self.after_transformation();
    }

    /// Override to update stuff after transformations
    /// 
    /// Exemple I have in mind is to allow for parenting objects that follow the modifications
    fn after_transformation(&mut self) {

    }
}

/// Allows entities to get positioned in 3D space relative to the camera
pub trait InCamera {
    fn transform(&mut self, camera_trans: &Transformation);

    fn after_camera_transformation(&mut self) {
        
    }
}
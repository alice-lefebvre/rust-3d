//! The module that takes care of importing OBJ files into the engine

use image::ColorType;
use material::{Texture, Material};

use crate::renderer::Shading;

use super::*;
use super::mesh::{Face, Mesh};

use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

/// Sorry but if your file does not follow the obj format, this function will panic
pub fn load_obj(path: &str) -> Result<Vec<Mesh>, std::string::String> {
    let mut objects = Vec::<Mesh>::new();
    let mut points = Vec::new();
    let mut faces = Vec::<Face>::new();
    let mut vertex_normals = Vec::new();
    let mut vertex_texture_coord = Vec::new();

    let mut vtx_id_offset = 0;
    let vt_id_offset = 0;
    // let mut vn_id_offset = 0;
    // let mut last_vn = 0;
    let mut last_point = 0;

    let mut materials_map = HashMap::<String, Material>::new();
    let mut use_mtl :Option<Material> = None;

    let f = File::open(path);
    let base_path = Path::new(path);
    let base_path = base_path.parent().unwrap();

    let f = match f {
        Ok(file) => file,
        Err(e) => return Err(format!("Cannot open the file {} : {}", path, e)),
    };

    println!("Loading {}...", path);

    let file_reader = io::BufReader::new(f);
    for line in file_reader.lines() {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }
        let line: Vec<&str> = line.split(' ').collect();
        // println!("{} : ", line);
        match line[0] {
            "mtllib" => {
                let mtllib_file = line[1];
                println!("  mtllib file linked");

                let mtllib_file = format!("{}/{}", base_path.to_str().unwrap(), mtllib_file);

                println!("  Loading {}...", mtllib_file);
                let f = File::open(mtllib_file);


                let f = match f {
                    Ok(file) => file,
                    Err(_e) => {
                        println!("    !! Could not open linked mtllib file");
                        println!("  Aborted.");
                        continue;
                    },
                };

                process_mtllib(f, &mut materials_map, base_path);

                println!("  Loaded {} materials:", materials_map.len());

                for key in materials_map.keys() {
                    println!("    {}", key.as_str());
                }


            }
            "v" => {
                points.push(Vec3::new(
                    line[1].parse::<f32>().unwrap(),
                    line[2].parse::<f32>().unwrap(),
                    line[3].parse::<f32>().unwrap(),
                ));

                last_point += 1;
            }
            "f" => {
                // let (a0, a2, a3) = (vec[1].split("/").collect(), vec[2].split("/").collect(), vec[3].split("/").collect());

                let a0: Vec<&str> = line[1].split('/').collect();
                let a1: Vec<&str> = line[2].split('/').collect();
                let a2: Vec<&str> = line[3].split('/').collect();
                let p0 = a0[0].parse::<usize>().unwrap() - 1 - vtx_id_offset;
                let p1 = a1[0].parse::<usize>().unwrap() - 1 - vtx_id_offset;
                let p2 = a2[0].parse::<usize>().unwrap() - 1 - vtx_id_offset;

                let mut face;

                if let Some(s) = use_mtl.as_ref() {
                    face = Face::new_with_material(p0, p1, p2, s.clone());
                } else {
                    face = Face::new(p0, p1, p2);
                }

                if a0.len() > 1 {
                    let vt0 = a0[1].parse::<usize>().unwrap() - 1 - vt_id_offset;
                    let vt1 = a1[1].parse::<usize>().unwrap() - 1 - vt_id_offset;
                    let vt2 = a2[1].parse::<usize>().unwrap() - 1 - vt_id_offset;

                    face.texture_coord = [vertex_texture_coord[vt0], vertex_texture_coord[vt1], vertex_texture_coord[vt2]];
                    // if panic here, texture coordinates without texture image
                    if face.material.texture.is_some() {
                        let height = face.material.texture.as_ref().unwrap().image.get_height();
                        let width = face.material.texture.as_ref().unwrap().image.get_width();

                        // we just care for negative values
                        let mut minx = 0.0_f32;
                        let mut miny = 0.0_f32;

                        

                        // correcting negative values by shitfting the 3 TC so that none of them is <0
                        for tc in face.texture_coord.iter_mut() {
                            tc.1 = 1.0-tc.1;
                            if tc.0 < minx {
                                minx = tc.0;
                            }
                            if tc.1 < miny {
                                miny = tc.1;
                            }
                        }

                        let offsetx = (-minx).floor() + 1.0;
                        let offsety = (-miny).floor() + 1.0;

                        let offset = f32::max(offsetx, offsety);

                        for tc in face.texture_coord.iter_mut() {

                            tc.0 += offset;
                            tc.1 += offset;

                            tc.0 *= width as f32;
                            tc.1 *= height as f32;

                        }
                }
                    
                }

                /*if a0.len() > 2 {
                    let vn0 = a0[2].parse::<usize>().unwrap() - 1 - vn_id_offset;
                    let vn1 = a1[2].parse::<usize>().unwrap() - 1 - vn_id_offset;
                    let vn2 = a2[2].parse::<usize>().unwrap() - 1 - vn_id_offset;

                    // face.vtx_normal_id = [vn0, vn1, vn2];
                }*/

                faces.push(face);
                
            }
            "usemtl" => {
                let color_opt = materials_map.get(line[1]);
                match color_opt {
                    None => println!("    !! The mesh uses the material \"{}\", but we don't have it indexed", line[1]),
                    Some(c) => use_mtl = Some(c.clone()),
                }
            },
            "o" => {  
                if !points.is_empty() {
                    let mut mesh = Mesh::new(points.into_boxed_slice(), faces.into_boxed_slice());
                    // mesh.set_vertex_normals(vertex_normals.clone());
                    if vertex_normals.is_empty() {
                        mesh.shading = Shading::Flat;
                    }
                    objects.push(mesh);
                    println!("  OK");
                }

                println!("  Loading object {}: {}", objects.len(), line[1]);

                vertex_normals = Vec::new();
                points = Vec::new();
                faces = Vec::<Face>::new();
                vtx_id_offset = last_point;
                // vn_id_offset = last_vn;
            },
            "vt" => {
                vertex_texture_coord.push((
                    line[1].parse::<f32>().unwrap(),
                    line[2].parse::<f32>().unwrap()));

                // last_vertex_texture_coord += 1;
            },
            "vn" => {
                vertex_normals.push(Vec3::new(
                    line[1].parse::<f32>().unwrap(),
                    line[2].parse::<f32>().unwrap(),
                    line[3].parse::<f32>().unwrap(),
                ));

                // last_vn += 1;
            }, //miam vertex normals
            "#" => (), // comment
            "s" => (), // idk what that is but it doesn't seem important 
            _ => println!("Unknown: {}", line[0]),
            // _ => println!("unknown : {}", line[0]),
        }
    }

    let mut mesh = Mesh::new(points.into_boxed_slice(), faces.into_boxed_slice());
    //mesh.set_vertex_normals(vertex_normals.clone());
    if vertex_normals.is_empty() {
        mesh.shading = Shading::Flat;
    }
    objects.push(mesh);
    println!("  OK");

    println!("{} loaded\n", path);

    Ok(objects)
}

fn process_mtllib(file: File, map: &mut HashMap<String, Material>, base_path: &Path) {
    let file_reader = io::BufReader::new(file);

    let mut material_name = String::new();
    let mut current_material= Material::default();
    let mut first_line = true;

    for line in file_reader.lines() {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }
        let line = line.replace('\t', "");
        let words: Vec<&str> = line.split(' ').collect();
        match words[0] {
            "newmtl" => {
                material_name = words[1].to_owned();
                current_material = Material::default();
                if !first_line {
                    map.insert(material_name.clone(), current_material.clone());
                }
                first_line = false;
            }
            "Kd" => {
                let r = words[1].parse::<f32>().unwrap();
                let g = words[2].parse::<f32>().unwrap();
                let b = words[3].parse::<f32>().unwrap();

                let color = Color::new(r, g, b);
                
                current_material.color = color;
            }
            "map_Kd" => {
                let txt_file = format!("{}/{}", base_path.to_str().unwrap(), words[1]);
                println!("{}", txt_file);
                let m = load_texture(txt_file.as_str());

                current_material.texture = m.texture;
            }

            _ => ()
        }

        map.insert(material_name.clone(), current_material.clone());
    }
}

pub fn load_texture(path: &str) -> Material {
    let img = image::open(path).unwrap();

    let texture = match img.color() {
        ColorType::Rgb8 => {
            let buf = img.as_rgb8().unwrap();
            let mut text_img = l2d::Buffer::new(buf.width(), buf.height());
            for (x, y, pixel) in buf.enumerate_pixels() {
                text_img.draw_pixel(&l2d::Pixel::new(x as i32, y as i32), l2d::Color::from_rgb(pixel.0[0], pixel.0[1], pixel.0[2]));
            }
            Texture::new(text_img)
        },
        ColorType::Rgba8 => {
            let buf = img.as_rgba8().unwrap();
            let mut text_img = l2d::Buffer::new(buf.width(), buf.height());
            for (x, y, pixel) in buf.enumerate_pixels() {
                text_img.draw_pixel(&l2d::Pixel::new(x as i32, y as i32), l2d::Color::from_rgb(pixel.0[0], pixel.0[1], pixel.0[2]));
            }
            Texture::new(text_img)
        },
        _ => {
            let mut text_img = l2d::Buffer::new(1, 1);
            text_img.fill(l2d::colors::MAGENTA);
            Texture::new(text_img)
        },
    };
    
    let mut material = material::Material { texture: Some(std::rc::Rc::new(texture)), color: colors::WHITE, ..Default::default() };
    material.color = colors::WHITE;
    material
}
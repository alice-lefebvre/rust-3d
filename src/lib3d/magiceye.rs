use crate::l2d::{self, Buffer, Color, Pixel};
use crate::mesh::Texture;


/// The depth map decides what the image size is gonna be but meh
pub fn render(image: &Buffer<Color>, depth_map: &Buffer<f32>, mapping: fn(f32) -> u32) -> Buffer<u32>{
    let width = depth_map.get_width();
    let height = depth_map.get_height();
    let mut offset_buffer: Buffer<u32> = Buffer::new(depth_map.get_width(), depth_map.get_height());
    for i in 0..depth_map.get_buffer().len() {
        offset_buffer.draw_pixel_raw_coord(&i, mapping(depth_map.get_buffer()[i]));
    }
    let mut current_image;
    let repeat_width = image.get_width();
    if image.get_height() >= height {
        current_image = image.clone(); //dont care if larger
    } else {
        current_image = Buffer::new(image.get_width(), height);
        for i in 0..(image.get_width() * height) as usize {
            current_image.get_buffer_mut()[i] = image.get_buffer()[i % image.get_buffer().len()];
        }
    }
    let mut render: Buffer<u32> = Buffer::new(width, depth_map.get_height());
    //ew
    for i in 0..current_image.get_width() {
        for j in 0..current_image.get_height() {
            render.draw_pixel(&Pixel::new(i as i32, j as i32), current_image.get_value_no_check(&Pixel::new(i as i32, j as i32)).value)
        }
    }
    for n in 1..width / (repeat_width -1) {
        // first copy over the image
        for i in n * current_image.get_width()..(n + 1)* current_image.get_width() {
            for j in 0..height {
                //render.draw_pixel(&Pixel::new(i as i32, j as i32), render.get_value_no_check(&Pixel::new((i - current_image.get_width()) as i32, j as i32)));
            }
        }
        // then displace the pixels
        for i in n * repeat_width..(n+1) * repeat_width {
            for j in 0..height {
                let offset = offset_buffer.get_value_no_check(&Pixel::new(i as i32, j as i32)); 
                let value = render.get_value_no_check(&Pixel::new((i - repeat_width) as i32, j as i32));
                let mut value_to_the_right = render.get_value_no_check(&Pixel::new(((i - repeat_width + 1)) as i32, j as i32));
                if value_to_the_right == 0 {
                    //if we run into the yet unrendered right side we use the first pixel of the image
                    //dirty check, pray the image doesn't have black blacks, but i don't wanna do clean stuff rn
                    value_to_the_right = current_image.get_value_no_check(&Pixel::new(0, j as i32)).value;
                }
                if offset != 0 {
                    for x in 1..offset+1 {
                        render.draw_pixel(&Pixel::new((i - offset + x) as i32, j as i32), l2d::Color::interpolate(&Color::new(value), &Color::new(value_to_the_right), (x as f32 / offset as f32)).value);
                        //println!("{:x}", l2d::Color::interpolate(&Color::new(value), &Color::new(value_to_the_right), (x as f32 / offset as f32)).value);
                    }
                        //println!("end");
                }
                    render.draw_pixel(&Pixel::new((i - offset) as i32, j as i32), value);
            }
        }
    }
    render
}

pub fn test_mapping(f: f32) -> u32 {
    let max_depth = 40;
    let mult = 3.0;
    
    let a = f * mult;
    if a as u32 > max_depth {return 0}
    //let a = if a > 5.0 {0.0} else {5.0 - a};
    max_depth - a as u32

    //if res > 5 {5} else {res}
}
//! The module that handles 3d geometry

use crate::{trans::Transformation, renderer::Renderable};
use std::collections::HashMap;
pub use crate::trans::{Transformable, InCamera};
pub use crate::material::*;
// use super::Color;
use crate::*;

pub trait HasVertices {
    fn get_vertices(&self) -> &[Vec3];
    fn get_vertices_transformed(&mut self) -> &mut [Vec3];
}

pub trait HasVertexNormals {
    fn get_vertex_normals(&self) -> &[Vec3];
    fn get_vertex_normals_transformed(&mut self) -> &mut [Vec3];
}

#[derive(Clone)]
pub struct Face {
    pub vtx_id: [usize; 3], //TODO: transformer ça en référence en fait ?????????????? qui a fait ça comme ça c'est idiot
    // (changer les choses pour que le point soit intégré dans la face et on transforme l'array de faces, peut être ?)
    pub normal: Vec3,
    pub normal_trans: Vec3,
    pub material: Material,
    pub texture_coord: [(f32, f32); 3],
}


impl Face {
    pub fn new(idx_1: usize, idx_2: usize, idx_3: usize) -> Face {
        let mut points_idx = [0_usize; 3];
        points_idx[0] = idx_1;
        points_idx[1] = idx_2;
        points_idx[2] = idx_3;

        Face {
            vtx_id: points_idx,
            normal: Vec3::new(0.0, 0.0, 0.0),
            normal_trans: Vec3::default(),
            material: Material::with_color(colors::WHITE),
            texture_coord: [(0.0, 0.0); 3],
        }
    }

    pub fn new_with_color(idx_1: usize, idx_2: usize, idx_3: usize, color: Color) -> Face {
        let mut points_idx = [0_usize; 3];
        points_idx[0] = idx_1;
        points_idx[1] = idx_2;
        points_idx[2] = idx_3;

        Face {
            vtx_id: points_idx,
            normal: Vec3::new(0.0, 0.0, 0.0),
            normal_trans: Vec3::default(),
            material: Material::with_color(color),
            texture_coord: [(0.0, 0.0); 3],
        }
    }

    pub fn new_with_material(idx_1: usize, idx_2: usize, idx_3: usize, material: Material) -> Face {
        let mut points_idx = [0_usize; 3];
        points_idx[0] = idx_1;
        points_idx[1] = idx_2;
        points_idx[2] = idx_3;

        Face {
            vtx_id: points_idx,
            normal: Vec3::new(0.0, 0.0, 0.0),
            normal_trans: Vec3::default(),
            material,
            texture_coord: [(0.0, 0.0); 3],
        }
    }
}

#[derive(Clone)]
pub struct BoundingBox {
    pub vertices: Box::<[Vec3]>,
    pub vertices_transformed: Box::<[Vec3]>,
    // pub lines: Vec<Line3d>,
    pub origin: Vec3,
    pub transformation: Transformation,
}

#[derive(Clone)]
pub struct Line3d {
    pub start: Box<Vec3>,
    pub end: Box<Vec3>,
}

impl Line3d {
    fn new() -> Line3d {
        Line3d {
            start: Box::new(Vec3::default()),
            end: Box::new(Vec3::default()),
        }
    }
}

impl Transformable for BoundingBox {
    fn get_transformation(&mut self) -> &mut Transformation {
        &mut self.transformation
    }
}

impl HasVertices for BoundingBox {
    fn get_vertices(&self) -> &[Vec3] {
        &self.vertices
    }

    fn get_vertices_transformed(&mut self) -> &mut [Vec3] {
        &mut self.vertices_transformed
    }
}

impl Renderable for BoundingBox {
    fn render(&self, renderer: &mut Renderer, _lighting: Option<&lighting::Lighting>) -> Option<u32> {
        for v in self.vertices_transformed.iter() {
            let pixel = renderer.project(v);
            if v.z > 0.0 && renderer.get_image().is_valid(&pixel) {
                renderer.get_image_mut().draw_big_pixel(pixel, 0xFF0000);
                renderer.get_depth_buffer_mut().draw_big_pixel(pixel, 0.0); // Always draw BB in front
            }
        }
        return Some(0)
    }
}

impl BoundingBox {
    pub fn new(vertices: Box::<[Vec3; 8]>, origin: Vec3) -> BoundingBox {
        BoundingBox{ 
            vertices,
            vertices_transformed: Box::new([Vec3::default(); 8]),
            origin,
            transformation: Transformation::default(),
        }
    }
    pub fn default() -> BoundingBox {
        BoundingBox::new(Box::new([Vec3::default(); 8]), Vec3::default())
    }
}

/// The struct that holds data of 3d objects
#[derive(Clone)]
pub struct Mesh {
    vertices: Box<[Vec3]>,
    vertices_trans: Box<[Vec3]>,
    vertex_normals: Box<[Vec3]>,
    vertex_normals_trans: Box<[Vec3]>,
    pub bounding_box: BoundingBox,
    transformation: Transformation,
    faces: Box<[Face]>,
    /// The point which indicate the origin of the mesh. This will be the center of rotation for instance
    origin: Vec3,
    pub shading: renderer::Shading,
	pub render_mode: renderer::RenderMode,
   	pub backface_culling: bool,
   	pub accurate_zbuffer:bool,
    pub hidden: bool,
    pub frustum_culling: bool, // can be useful to disable culling for large objects bc the way it
                               // works (maybe check for this case in frustum culling)
    pub draw_bb: bool,
    
}

impl Mesh {
    pub fn new(vertices: Box<[Vec3]>, faces: Box<[Face]>) -> Mesh {
        let mut obj = Mesh {
            vertices: vertices.clone(),
            vertices_trans: vertices.clone(),
            vertex_normals: vec![Vec3::default(); vertices.len()].into_boxed_slice(),
            vertex_normals_trans: vec![Vec3::default(); vertices.len()].into_boxed_slice(),
            bounding_box: BoundingBox::default(),
            transformation: Transformation::new(Vec3::default(), Mat3::identity()),
            faces,
            origin: Vec3::new(0.0, 0.0, 0.0),
            shading: renderer::Shading::Smooth,
            render_mode: renderer::RenderMode::Solid,
            backface_culling: true,
            accurate_zbuffer: true,
            hidden: false,
            frustum_culling: false, // disable culling bc doesn't work for v large objects.
            // very little perf. gain it seems (bc we don't draw outside the camera anyway
            draw_bb: false,
        };
        
        obj.set_origin_auto();
        
        // make this optional at some point
        //obj.translate_points(Vec3::default() - obj.center_of_rotation);
        //obj.translate(obj.center_of_rotation);

        obj.bake_face_normals();
        obj.bake_vertex_normals();
        obj.calculate_bounding_box();
        obj
    }

    pub fn empty() -> Mesh {
        Mesh::new(Box::new([]), Box::new([]))
    }

    pub fn set_vertex_normals(&mut self, vertex_normals: Box<[Vec3]>) {
        self.vertex_normals = vertex_normals;
    }

    pub fn get_vertex_normals(&self) -> &Box<[Vec3]> {
        &self.vertex_normals_trans
    }

    pub fn merge_meshes(mut meshes: Vec::<Mesh>) -> Mesh {
        let mut point_offset = 0;
        let mut points = Vec::new();
        let mut faces = Vec::new();

        for m in meshes.iter_mut() {
            // m.transform(&Transformation::default()); // to account for rotations, translations etc of the model
            let mut _points = m.get_vertices().clone().to_vec();
            let mut _faces = m.get_faces().clone().to_vec();

            points.append(&mut _points);

            for f in _faces.iter_mut() {
                f.vtx_id[0] += point_offset;
                f.vtx_id[1] += point_offset;
                f.vtx_id[2] += point_offset;
            }

            faces.append(&mut _faces);



            point_offset = points.len();
        };

        Mesh::new(points.into_boxed_slice(), faces.into_boxed_slice())
    }

    pub fn apply_transformations_to_geometry(&mut self) {
        for p in self.vertices.iter_mut() {
            *p = *p - self.origin;
            *p = self.transformation.rotation * (*p);
            *p = *p + self.transformation.translation + self.origin;
        }

        self.transformation = Transformation::default();

        self.bake_face_normals();
        self.bake_vertex_normals();
        self.calculate_bounding_box();
    }

    pub fn merge_vertices(&mut self, threshold: f32) {
        let mut indices_remap = HashMap::<usize, usize>::new();

        let mut vertices_merged = Vec::new();

        // let mut vertices_to_check_against = Vec::new();
        

        for (i, v) in self.vertices.iter().enumerate() {
            let mut merged = false;
            for (j, k) in vertices_merged.iter().enumerate() {
                if Vec3::distance(v, k) < threshold {
                    merged = true;
                    indices_remap.insert(i, j);
                    break;
                }
            }
            if !merged {
                vertices_merged.push(*v);
                indices_remap.insert(i, vertices_merged.len() - 1);
            }
        }

        for f in self.faces.iter_mut() {
            for v in f.vtx_id.iter_mut() {
                if indices_remap.contains_key(v) {
                    *v = *indices_remap.get(v).unwrap();
                }
            }
        }

        //println!("# merging vertices");
        //println!("nb of vertices before: {}", self.vertices.len());
        //println!("nb of vertices after : {}", vertices_merged.len());

        self.vertices = vertices_merged.into_boxed_slice();
        self.vertices_trans = self.vertices.clone();


    }
    
    pub fn bake_face_normals(&mut self) {
    	for mut t in self.faces.iter_mut() {
    		let a = self.vertices[t.vtx_id[0]];
            let b = self.vertices[t.vtx_id[1]];
            let c = self.vertices[t.vtx_id[2]];
    		
    		t.normal = Vec3::find_normal(a, b, c);
    	}
    }

    pub fn bake_vertex_normals(&mut self) {
        // self.vertex_normals = Vec::with_capacity();
        self.vertex_normals = vec![Vec3::default(); self.vertices.len()].into_boxed_slice();

        for f in self.faces.iter() {
            let a = self.vertices[f.vtx_id[0]];
            let b = self.vertices[f.vtx_id[1]];
            let c = self.vertices[f.vtx_id[2]];

            let p = Vec3::cross_product(b - a, c - a);

            for i in 0..3 {
                self.vertex_normals[f.vtx_id[i]] = self.vertex_normals[f.vtx_id[i]] + p;
            }
        }

        for vn in self.vertex_normals.iter_mut() {
            vn.normalize();
        }

    }

    pub fn flip_normals(&mut self) {
        for t in self.faces.iter_mut() {
            t.normal.invert();
        }

        for t in self.vertex_normals.iter_mut() {
            t.invert();
        }
    }

    pub fn get_vertices(&self) -> &Box<[Vec3]> {
        &self.vertices
    }

    pub fn get_vertices_mut(&mut self) -> &mut Box<[Vec3]> {
        &mut self.vertices
    }

    pub fn get_vertices_for_rendering(&self) -> &Box<[Vec3]> {
        &self.vertices_trans
    }

    pub fn get_faces(&self) -> &Box<[Face]> {
        &self.faces
    }

    pub fn paint_all(&mut self, color: Color) {
        for face in self.faces.iter_mut() {
            face.material.color = color;
        }
    }

    pub fn set_material(&mut self, material: Material) {
        for face in self.faces.iter_mut() {
            face.material = material.clone();
        }
    }

    pub fn get_faces_mut(&mut self) -> &mut Box<[Face]> {
        &mut self.faces
    }

    pub fn get_triangle_count(&self) -> u32 {
        self.faces.len() as u32
    }

    pub fn apply_fn_to_points(&mut self, f: fn(Vec3, f32) -> Vec3, arg: f32) {
        for i in 0..self.vertices.len() {
            self.vertices[i] = f(self.vertices[i], arg);
        }
    }


    pub fn scale(&mut self, vec: (f32, f32, f32)) {
        // tout pourri mais on le fait pas souvent
        let cor = self.origin;
        translate_points(&mut self.vertices, Vec3::new(-cor.x, -cor.y, -cor.z));
        translate_points(&mut self.bounding_box.vertices, Vec3::new(-cor.x, -cor.y, -cor.z));
        
        for p in self.vertices.iter_mut() {
            p.x *= vec.0;
            p.y *= vec.1;
            p.z *= vec.2;
        }

        for p in self.bounding_box.vertices.iter_mut() {
            p.x *= vec.0;
            p.y *= vec.1;
            p.z *= vec.2;
        }

        translate_points(&mut self.vertices, Vec3::new(cor.x, cor.y, cor.z));
        translate_points(&mut self.bounding_box.vertices, Vec3::new(cor.x, cor.y, cor.z));
    }

    /// Get center of the object
    pub fn get_center(&self) -> Vec3 {
        let mut x_max = f32::MIN;
        let mut y_max = f32::MIN;
        let mut z_max = f32::MIN;
        let mut x_min = f32::MAX;
        let mut y_min = f32::MAX;
        let mut z_min = f32::MAX;

        for p in self.vertices.iter() {
            if p.x > x_max {
                x_max = p.x
            };
            if p.y > y_max {
                y_max = p.y
            };
            if p.z > z_max {
                z_max = p.z
            };
            if p.x < x_min {
                x_min = p.x
            };
            if p.y < y_min {
                y_min = p.y
            };
            if p.z < z_min {
                z_min = p.z
            };
        }

        Vec3::new(
            (x_max + x_min) / 2.0,
            (y_max + y_min) / 2.0,
            (z_max + z_min) / 2.0,
        )
    }

    /// Get center of gravity of the object
    pub fn get_cog(&self) -> Vec3 {
        let mut x_values = 0_f32;
        let mut y_values = 0_f32;
        let mut z_values = 0_f32;

        for p in self.vertices.iter() {
            x_values += p.x;
            y_values += p.y;
            z_values += p.z;
        }

        let size = self.vertices.len() as f32;

        Vec3::new(x_values / size, y_values / size, z_values / size)
    }

    pub fn set_origin(&mut self, cor: Vec3) {
        self.origin = cor;
        self.bounding_box.origin = cor;
    }

    ///
    pub fn set_origin_auto(&mut self) {
        self.set_origin(self.get_center());
    }

    /// creates a bouding box aligned on the X, Y and Z axis
    pub fn calculate_bounding_box(&mut self) {
        let mut bounds = [(f32::MAX, f32::MIN); 3]; // bounds[0] = x, 2 = y, 3 = Z. bounds[0].0 = min value for X
        for v in self.get_vertices().iter() {
            if v.x < bounds[0].0 {
                bounds[0].0 = v.x;
            }
            if v.x > bounds[0].1 {
                bounds[0].1 = v.x;
            }
            if v.y < bounds[1].0 {
                bounds[1].0 = v.y;
            }
            if v.y > bounds[1].1 {
                bounds[1].1 = v.y;
            }
            if v.z < bounds[2].0 {
                bounds[2].0 = v.z;
            }
            if v.z > bounds[2].1 {
                bounds[2].1 = v.z;
            }
        }
        let mut vertices = [Vec3::default(); 8];

        // that should make a cube ??? or at least its vertices
        for i in 0..8 {
            // i *will* spend 10 minutes writing a weird binary thing than writing all the vertices by hand in 5 minutes
            vertices[i].x = if  i & 0b001       == 1 { bounds[0].1 } else { bounds[0].0 };
            vertices[i].y = if (i & 0b010) >> 1 == 1 { bounds[1].1 } else { bounds[1].0 };
            vertices[i].z = if (i & 0b100) >> 2 == 1 { bounds[2].1 } else { bounds[2].0 };
        }

        self.bounding_box = BoundingBox::new(Box::new(vertices), self.origin);
    }
}

//translate directement la géométrie. à éviter. Utile pour changer le centre de rotation
pub fn translate_points(vertices: &mut [Vec3], vec: Vec3) {
    for p in vertices.iter_mut() {
        p.x += vec.x;
        p.y += vec.y;
        p.z += vec.z;
    }
}

impl Transformable for Mesh {
    fn get_transformation(&mut self) -> &mut Transformation {
        &mut self.transformation
    }

    fn after_transformation(&mut self) {
        self.bounding_box.transformation = self.transformation.clone();
        // todo!("apply transformation to bounding box somehow");
    }
}

impl InCamera for Mesh {
    fn transform(&mut self, camera_trans: &Transformation) {
        // vertices
        transform_vertices_in_camera(&self.vertices, &mut self.vertices_trans, &self.origin, &self.transformation, camera_trans);
        //normals
        for f in self.faces.iter_mut() {
            let mut n = f.normal;
            //mesh
            n = self.transformation.rotation * n;
            //camera
            n = camera_trans.rotation * n;
            f.normal_trans = n;
        }
        // vertex normals
        tranform_normalized_vectors_in_camera(&self.vertex_normals, &mut self.vertex_normals_trans, &self.transformation, camera_trans);

        //bounding box follows the motion of the object
        self.bounding_box.transform(camera_trans);
    }
}

impl InCamera for BoundingBox {
   fn transform(&mut self, camera_trans: &Transformation) {
       transform_vertices_in_camera(&self.vertices, &mut self.vertices_transformed, &self.origin, &self.transformation, camera_trans)
   } 
}

/// Applies a transformation and a camera transformation to a vertex array
pub fn transform_vertices_in_camera(vertices: &[Vec3], vertices_transformed: &mut [Vec3], origin: &Vec3, transformation: &Transformation, camera_trans: &Transformation) {
    for (i, p) in vertices.iter().enumerate() {
        let mut p = *p;
        //mesh
        p = p - *origin;
        p = transformation.rotation * p;
        p = p + transformation.translation;
        p = p + *origin;
        //camera
        p = p + camera_trans.translation;
        p = camera_trans.rotation * p;
        vertices_transformed[i] = p;
    }
}

pub fn tranform_normalized_vectors_in_camera(vectors: &[Vec3], vectors_transformed: &mut [Vec3], transformation: &Transformation, camera_trans: &Transformation) {
    for (i, p) in vectors.iter().enumerate() {
        let mut p = *p;
        //mesh
        p = transformation.rotation * p;
        //camera
        p = camera_trans.rotation * p;
        vectors_transformed[i] = p;
    }
}

impl Renderable for Mesh {
    fn render(&self, renderer: &mut Renderer, lighting: Option<&lighting::Lighting>) -> Option<u32>  {
        let tris = renderer.draw_3d_mesh(&self, lighting);
        if self.draw_bb {
            self.bounding_box.render(renderer, lighting);
        }
        return tris
    }

    fn get_shading_mut(&mut self) -> Option<&mut renderer::Shading> {
        Some(&mut self.shading)
    }

    fn get_render_mode_mut(&mut self) -> Option<&mut renderer::RenderMode> {
        Some(&mut self.render_mode)
    }
}

impl HasVertices for Mesh {
    fn get_vertices(&self) -> &[Vec3] {
        &self.vertices
    }

    fn get_vertices_transformed(&mut self) -> &mut [Vec3] {
        &mut self.vertices_trans
    }
}

impl HasVertexNormals for Mesh {
    fn get_vertex_normals(&self) -> &[Vec3] {
        &self.vertex_normals
    }

    fn get_vertex_normals_transformed(&mut self) -> &mut [Vec3] {
        &mut self.vertex_normals_trans
    }
}
